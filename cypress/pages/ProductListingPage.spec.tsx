import React from 'react';
import { composeStories } from '@storybook/testing-react';
import { mount } from '@cypress/react';
import * as stories from '@stories/pages/ProductListingPage.stories';
import { ReactQueryDecorator } from '@utils/storybook/decorators';

const { Default } = composeStories(stories, {
  decorators: [ReactQueryDecorator]
});
describe('Default', () => {
  it('should change button text on click', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findAllByRole('button', { name: /Add to wishlist/i })
      .first()
      .click();

    cy.findAllByRole('button', { name: /remove from wishlist/i }).first();
  });

  it('should open drawer', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findByText('Wishlist').should('exist').click();

    cy.findByText('My Wishlist').should('exist');
  });

  it('should add item in wishlist', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findAllByRole('button', { name: /Add to wishlist/i })
      .first()
      .click();
    cy.findByLabelText('wish-list-total-items').should('exist');
    cy.findByText('Wishlist').should('exist').click();

    cy.findByText('My Wishlist').should('exist');
    cy.findByLabelText('wish-list-item').should('exist');
  });

  it('should show a badge', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findAllByRole('button', { name: /Add to wishlist/i })
      .first()
      .click();
    cy.findByLabelText('wish-list-total-items').should('exist');
  });

  it('should show a badge', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findAllByRole('button', { name: /Add to wishlist/i })
      .first()
      .click();
    cy.findByLabelText('wish-list-total-items').should('exist');
  });
});
