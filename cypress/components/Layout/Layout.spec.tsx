import React from 'react';
import { composeStories } from '@storybook/testing-react';
import { mount } from '@cypress/react';
import * as stories from '@stories/components/organism/Layout.stories';

const { Default } = composeStories(stories);
describe('Default', () => {
  it('should render layout', () => {
    cy.viewport('macbook-16');
    mount(<Default />);
    cy.findAllByRole('button', { name: /Add to wishlist/i }).should(
      'have.lengthOf',
      10
    );
    cy.findAllByRole('img').should('have.lengthOf', 10);
    cy.findAllByText(/Card title/i).should('exist');
    cy.findAllByText(/This is a description/i).should('exist');
  });
});
