/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

/**
 * @type {Cypress.PluginConfig}
 */

// module.exports = (on, config) => {
//   if (config.testingType === 'component') {
//     const { startDevServer } = require('@cypress/webpack-dev-server')
//     // Your project's Webpack configuration
//     // const webpackConfig = require('../../webpack.test.config.ts')
//     on('dev-server:start', (options) =>
//       startDevServer({ options })
//     )
//   }
// }
import { startDevServer } from '@cypress/webpack-dev-server';
import webpackConfig from '../../webpack.dev.config';

module.exports = (on: Cypress.PluginEvents, config: Cypress.PluginConfigOptions) => {
  on('dev-server:start', async (options) => startDevServer({ options, webpackConfig }))
  return config;
}