import { Header } from '@components/organism/Header/Header';
import { Layout } from '@components/organism/Layout/Layout';
import { ProductList } from '@components/organism/ProductList/ProductList';
import { WishList } from '@components/organism/WishList/WishList';
import { useProducts } from '@services/useProducts';
import { useWishList } from '@services/useWishList';
import { Badge, Button } from 'antd';
import React from 'react';
import { getPopulatedWishListItems } from './utils/getPopulatedWishListItems';
import { getWishlistItemTotalAmount } from './utils/getWishlistItemTotalAmount';

export const ProductListingPage = () => {
  const { isLoading, data: products } = useProducts();
  const { addToWishList, removeFromWishList, wishListItems, changeItemAmount } =
    useWishList();

  const itemsForWishList = products
    ? getPopulatedWishListItems(wishListItems, products)
    : [];

  return (
    <Layout
      drawerTitle="My Wishlist"
      drawerContent={
        <WishList onChange={changeItemAmount} items={itemsForWishList} />
      }
      header={(toggleDrawer) => (
        <Header
          title="Shopper"
          buttons={[
            <span aria-label="wish-list-total-items">
              <Badge count={getWishlistItemTotalAmount(wishListItems)}>
                <Button aria-label="open-my-wishlist" onClick={toggleDrawer}>
                  Wishlist
                </Button>
              </Badge>
            </span>
          ]}
        />
      )}
    >
      <ProductList
        products={products}
        isLoading={isLoading}
        removeFromWishlist={removeFromWishList}
        addToWishlist={addToWishList}
        productsOnWishList={wishListItems.map((item) => item.id)}
      />
    </Layout>
  );
};
