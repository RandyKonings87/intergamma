import { IWistListStoreItem } from '@services/useWishList';

export const getWishlistItemTotalAmount = (
  wishListItems: IWistListStoreItem[]
): number => {
  return wishListItems.reduce<number>(
    (totalAmount, item) => (totalAmount = totalAmount + item.amount),
    0
  );
};
