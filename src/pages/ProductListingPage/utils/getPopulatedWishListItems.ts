import { IProduct } from '@components/molecules/Product/Product';
import { IWishListItem } from '@components/molecules/WishListItem/WishListItem';
import { IWistListStoreItem } from '@services/useWishList';

export const getPopulatedWishListItems = (
  items: IWistListStoreItem[],
  products: IProduct[]
): IWishListItem[] => {
  return items.reduce<IWishListItem[]>((wishListItems, item) => {
    const product = products.find((product) => product.id === item.id);
    return product
      ? [...wishListItems, { ...product, amount: item.amount }]
      : wishListItems;
  }, []);
};
