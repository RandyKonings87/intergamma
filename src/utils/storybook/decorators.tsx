import React from 'react';
import { Story } from '@storybook/react';
import { QueryClient, QueryClientProvider } from 'react-query';

export const ReactQueryDecorator = (Story: Story) => {
  const queryClient = new QueryClient();
  return (
    <QueryClientProvider client={queryClient}>
      <Story />
    </QueryClientProvider>
  );
};
