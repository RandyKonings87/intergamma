import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app/App';
import '@styles/main.scss';
import 'antd/dist/antd.css';
import { QueryClient, QueryClientProvider } from 'react-query';
const queryClient = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
