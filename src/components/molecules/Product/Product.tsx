import React from 'react';
import { Card as InnerCard } from 'antd';
import Meta from 'antd/lib/card/Meta';
import { WishListButton } from '@components/atoms/WishListButton/WishListButton';

export interface IProduct {
  title: string;
  id: string;
  description: string;
  image: string;
}

export interface ProductProps extends IProduct {
  addToWishlist: (productId: IProduct['id']) => void;
  removeFromWishList: (productId: IProduct['id']) => void;
  onWishList: boolean;
}

export function Product({
  title,
  image,
  description,
  id,
  addToWishlist,
  removeFromWishList,
  onWishList
}: ProductProps) {
  return (
    <InnerCard
      cover={<img alt={title} src={image} />}
      actions={[
        <WishListButton
          key="WishListButton"
          addToWishList={() => addToWishlist(id)}
          removeFromWishList={() => removeFromWishList(id)}
          onWishList={onWishList}
        />
      ]}
    >
      <Meta title={title} description={description} />
    </InnerCard>
  );
}
