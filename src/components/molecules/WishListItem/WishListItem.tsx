import React from 'react';
import { Button, InputNumber, List } from 'antd';
import { IProduct } from '../Product/Product';
import Avatar from 'antd/lib/avatar/avatar';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';

export interface IWishListItem extends IProduct {
  amount: number;
}

export interface WishListItemProps extends IWishListItem {
  onChange: (id: string, amount: number) => void;
}

export function WishListItem({
  title,
  image,
  description,
  id,
  amount,
  onChange
}: WishListItemProps) {
  const actions = [
    <Button
      key="substract-amount"
      type="text"
      onClick={() => onChange(id, amount > 0 ? amount - 1 : 0)}
      icon={<MinusCircleOutlined />}
    />,
    <InputNumber
      key="input-amount"
      min={0}
      formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
      value={amount}
      onChange={(value) => onChange(id, value)}
    />,
    <Button
      key="add-amount"
      type="text"
      onClick={() => onChange(id, amount + 1)}
      icon={<PlusCircleOutlined />}
    />
  ];
  return (
    <List.Item aria-label="wish-list-item" actions={actions}>
      <List.Item.Meta
        avatar={<Avatar src={image} />}
        title={title}
        description={description}
      />
    </List.Item>
  );
}
