import { Button, Tooltip } from 'antd';
import React from 'react';

export interface WishListButtonProps {
  onWishList: boolean;
  addToWishList: () => void;
  removeFromWishList: () => void;
}
type AddToWishListButtonProps = Pick<WishListButtonProps, 'addToWishList'>;
const AddToWishListButton = ({ addToWishList }: AddToWishListButtonProps) => {
  return (
    <Tooltip title="Add to wishlist">
      <Button onClick={addToWishList}>Add to wishlist</Button>
    </Tooltip>
  );
};

type RemoveFromWishListButtonProps = Pick<
  WishListButtonProps,
  'removeFromWishList'
>;
const RemoveFromWishListButton = ({
  removeFromWishList
}: RemoveFromWishListButtonProps) => {
  return (
    <Tooltip title="Remove from wishlist">
      <Button onClick={removeFromWishList}>Remove from wishlist</Button>
    </Tooltip>
  );
};

export const WishListButton = ({
  onWishList,
  addToWishList,
  removeFromWishList
}: WishListButtonProps) => {
  return onWishList ? (
    <RemoveFromWishListButton removeFromWishList={removeFromWishList} />
  ) : (
    <AddToWishListButton addToWishList={addToWishList} />
  );
};
