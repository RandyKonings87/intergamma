import { PageHeader } from 'antd';
import React from 'react';
import styled from 'styled-components';

export interface HeaderProps {
  title?: string;
  className?: string;
  buttons?: React.ReactNode;
}

export const Header = styled(
  ({ title = 'Title', className, buttons }: HeaderProps) => {
    return <PageHeader className={className} title={title} extra={buttons} />;
  }
)``;
