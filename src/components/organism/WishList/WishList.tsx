import {
  IWishListItem,
  WishListItem
} from '@components/molecules/WishListItem/WishListItem';
import { List } from 'antd';
import React from 'react';

export interface WishListProps {
  items: IWishListItem[];
  onChange: (id: string, amount: number) => void;
}

export const WishList = ({ items, onChange }: WishListProps) => {
  return (
    <List
      dataSource={items}
      renderItem={(item) => (
        <WishListItem key={item.id} {...item} onChange={onChange} />
      )}
    />
  );
};
