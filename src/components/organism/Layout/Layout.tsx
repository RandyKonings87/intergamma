import { Drawer } from 'antd';
import React, { useState } from 'react';
import { Layout as AntdLayout } from 'antd';

export interface LayoutProps {
  children: JSX.Element;
  drawerContent: JSX.Element;
  drawerTitle: string;
  header?: (toggleDrawer: () => void) => JSX.Element;
}

export const Layout = ({
  children,
  drawerContent,
  drawerTitle,
  header
}: LayoutProps) => {
  const [isDrawerVisible, setIsDrawerVisible] = useState(false);
  const showDrawer = () => {
    setIsDrawerVisible(true);
  };
  const closeDrawer = () => {
    setIsDrawerVisible(false);
  };

  const toggleDrawer = () => {
    isDrawerVisible ? closeDrawer() : showDrawer();
  };

  return (
    <AntdLayout>
      {header?.(toggleDrawer)}
      <AntdLayout.Content>
        <AntdLayout>{children}</AntdLayout>
      </AntdLayout.Content>
      <AntdLayout.Footer>Footer</AntdLayout.Footer>
      <Drawer
        title={drawerTitle}
        placement="right"
        onClose={closeDrawer}
        visible={isDrawerVisible}
        width={600}
      >
        {drawerContent}
      </Drawer>
    </AntdLayout>
  );
};
