import React from 'react';
import { List } from 'antd';
import { IProduct, Product } from '@components/molecules/Product/Product';

function isProductOnWishList(
  productId: IProduct['id'],
  productsOnWishList: IProduct['id'][]
): boolean {
  return productsOnWishList.length > 0
    ? productsOnWishList.includes(productId)
    : false;
}

export interface ProductListProps {
  products?: IProduct[];
  addToWishlist: (productId: IProduct['id']) => void;
  removeFromWishlist: (productId: IProduct['id']) => void;
  productsOnWishList: IProduct['id'][];
  isLoading?: boolean;
}

export function ProductList({
  products,
  addToWishlist,
  removeFromWishlist,
  productsOnWishList,
  isLoading
}: ProductListProps) {
  return (
    <List
      grid={{
        gutter: 16,
        xs: 1,
        sm: 2,
        md: 4,
        lg: 4,
        xl: 6,
        xxl: 6
      }}
      dataSource={products}
      loading={isLoading}
      renderItem={(product, index) => (
        <List.Item key={index}>
          <Product
            {...product}
            removeFromWishList={removeFromWishlist}
            addToWishlist={addToWishlist}
            onWishList={isProductOnWishList(product.id, productsOnWishList)}
          />
        </List.Item>
      )}
    />
  );
}
