import React from 'react';
import { ProductListingPage } from 'src/pages/ProductListingPage/ProductListingPage';

function App() {
  return <ProductListingPage />;
}

export default App;
