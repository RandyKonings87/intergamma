import { IProduct } from '@components/molecules/Product/Product';
import { useQuery } from 'react-query';
import products from './data/products.json';

export const useProducts = () => {
  return useQuery(['products'], async () => {
    return products.map<IProduct>(({ id, title, description, filename }) => ({
      id,
      description,
      title,
      image: filename
    }));
  });
};
