import createPersistedState from 'use-persisted-state';

const STORAGE_KEY = 'WISH_LIST_STORE';

export interface IWistListStoreItem {
  id: string;
  amount: number;
}

const useWishListPresistedState =
  createPersistedState<IWistListStoreItem[]>(STORAGE_KEY);

export const useWishList = () => {
  const [wishListItems, setWishListItems] = useWishListPresistedState([]);

  const removeFromWishList = (id: string) => {
    setWishListItems((items) => items.filter((item) => item.id !== id));
  };

  const addToWishList = (id: string) => {
    setWishListItems((items) => [...items, { id, amount: 1 }]);
  };

  const changeItemAmount = (id: string, amount: number) => {
    if (amount === 0) {
      removeFromWishList(id);
    } else {
      setWishListItems((items) => {
        const updatedItems = items.map((item) =>
          item.id === id ? { ...item, amount } : item
        );

        return updatedItems;
      });
    }
  };

  return { wishListItems, removeFromWishList, addToWishList, changeItemAmount };
};
