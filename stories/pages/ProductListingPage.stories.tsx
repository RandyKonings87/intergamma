import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ProductListingPage } from 'src/pages/ProductListingPage/ProductListingPage';
import { ReactQueryDecorator } from '@utils/storybook/decorators';

export default {
  title: 'Pages/ProductListing',
  component: ProductListingPage
} as ComponentMeta<typeof ProductListingPage>;

const Template: ComponentStory<typeof ProductListingPage> = () => (
  <ProductListingPage />
);

export const Default = Template.bind({});
Default.args = {};
Default.decorators = [ReactQueryDecorator];
