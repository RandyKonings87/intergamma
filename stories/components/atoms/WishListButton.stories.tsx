import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { WishListButton } from '@components/atoms/WishListButton/WishListButton';
import { action } from '@storybook/addon-actions';
export default {
  title: 'Atoms/WishListButton',
  component: WishListButton
} as ComponentMeta<typeof WishListButton>;

const Template: ComponentStory<typeof WishListButton> = (args) => (
  <WishListButton {...args} />
);

const handlers = {
  addToWishList: action('addToWishList'),
  removeFromWishList: action('removeFromWishList')
};

export const Default = Template.bind({});
Default.args = {
  onWishList: false,
  ...handlers
};

export const OnWishList = Template.bind({});
OnWishList.args = {
  onWishList: true,
  ...handlers
};
