import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { WishListItem } from '@components/molecules/WishListItem/WishListItem';
import { action } from '@storybook/addon-actions';

export default {
  title: 'Molecules/WishListItem',
  component: WishListItem
} as ComponentMeta<typeof WishListItem>;

const Template: ComponentStory<typeof WishListItem> = (args) => (
  <WishListItem {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  title: 'Card title',
  description: 'This is a description',
  image: 'https://bit.ly/2Z4KKcF',
  amount: 10,
  onChange: action('onChange')
};
