import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Product } from '@components/molecules/Product/Product';

export default {
  title: 'Molecules/Product',
  component: Product
} as ComponentMeta<typeof Product>;

const Template: ComponentStory<typeof Product> = (args) => (
  <Product {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  title: 'Card title',
  description: 'This is a description',
  image: 'https://bit.ly/2Z4KKcF',
  addToWishlist: action('addToWishlist'),
  removeFromWishList: action('removeFromWishList')
};
