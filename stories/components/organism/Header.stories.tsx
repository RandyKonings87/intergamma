import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Header } from '@components/organism/Header/Header';

export default {
  title: 'Organism/Header',
  component: Header,
  argTypes: {
    backgroundColor: { control: 'color' }
  }
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => <Header {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  title: 'Header title'
};
