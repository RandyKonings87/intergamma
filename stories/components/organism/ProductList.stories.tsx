import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { ProductList } from '@components/organism/ProductList/ProductList';
import { action } from '@storybook/addon-actions';
export default {
  title: 'Organism/ProductList',
  component: ProductList
} as ComponentMeta<typeof ProductList>;

const Template: ComponentStory<typeof ProductList> = (args) => (
  <ProductList {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  productsOnWishList: [],
  addToWishlist: action('addToWishlist'),
  removeFromWishlist: action('removeFromWishlist'),
  products: Array(10)
    .fill(null)
    .map((_, index) => ({
      title: 'Card title',
      description: 'This is a description',
      image: 'https://bit.ly/2Z4KKcF',
      id: index.toString()
    }))
};

export const ProductsOnWishList = Template.bind({});
ProductsOnWishList.args = {
  ...Primary.args,
  productsOnWishList: ['1', '2', '5']
};
