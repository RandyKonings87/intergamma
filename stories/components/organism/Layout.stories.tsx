import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Layout } from '@components/organism/Layout/Layout';
import { ProductList } from '@components/organism/ProductList/ProductList';

export default {
  title: 'Organism/Layout',
  component: Layout
} as ComponentMeta<typeof Layout>;

const Template: ComponentStory<typeof Layout> = (args) => <Layout {...args} />;

export const Default = Template.bind({});
Default.args = {
  drawerContent: <p>Content of the drawer.</p>,
  drawerTitle: 'Title of the drawer',
  children: (
    <ProductList
      addToWishlist={() => null}
      productsOnWishList={[]}
      removeFromWishlist={() => null}
      products={Array(10)
        .fill(null)
        .map((_, index) => ({
          title: 'Card title',
          description: 'This is a description',
          image: 'https://bit.ly/2Z4KKcF',
          id: index.toString()
        }))}
    />
  )
};
