import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';
import { WishList } from '@components/organism/WishList/WishList';
import { action } from '@storybook/addon-actions';
export default {
  title: 'Organism/WishList',
  component: WishList
} as ComponentMeta<typeof WishList>;

const Template: ComponentStory<typeof WishList> = (args) => (
  <WishList {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
  onChange: action('item amount changed'),
  items: Array(10)
    .fill(null)
    .map((_, index) => ({
      title: 'Card title',
      description: 'This is a description',
      image: 'https://bit.ly/2Z4KKcF',
      id: index.toString(),
      amount: 2
    }))
};
