# README

This repository is made by Randy Konings for Intergamma. A Single page app build with React, Typescript, React Query. Developed with component driven developement in Storybook.

### What is this repository for?

This is for Intergamma to show my skills and experience as a Front-end developer.

- Version 1.0.0

### Where can I find the app

- You can preview the app at
- You can preview storybook at https://intergamma-storybook.vercel.app

### How do I get set up?

- Clone the repo
- Install npm and node (developed with node version 16)
- use npm install
- use npm start to start the app
- use npm run storybook to start storybook
- use npm test to start the Cypress component testing
- Deployment instructions

### What I like to do next if I had time?

- Writing testing to cover all use cases
- Implement a better design for better UX
- Fix minor errors in my config
- Add price to products
